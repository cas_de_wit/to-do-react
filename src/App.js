import './index';
import { useState } from 'react';

const Todo = ({ todo, onDelete }) => {

    return (
        <div className="todoCard">
            <p className="todoText">{todo.text}</p>
            <button onClick={() => onDelete(todo.id)} className="btn-delete">x</button>
        </div>
    )
}

const AddTodo = () => {
    const [todo, setTodo] = useState('');
    const [list, setList] = useState([
        {
            id: 1,
            text: 'Grasmaaien',
        },
        {
            id: 2,
            text: 'Koken',
        },
        {
            id: 3,
            text: 'Boodschappen doen',
        },
    ]);

    const handleSumbit = e => {
        e.preventDefault();
        // Check if the form has a value
        if (!todo) {
            return;
        }
        // Create id for todo
        const id = Math.floor(Math.random() * 10000 + 1);
        // Add new to do to list including an id
        setList([...list, { id: id, text: todo }])

        setTodo('');
    }
    // Filters the list, if the id is not equal to the id that gets passed in 'Todo' it will stay in the list, otherwise it will be removed.
    const deleteTodo = id => {
        setList(list.filter((todo) => todo.id !== id))
    }

    return (
        // Maps through the list and makes todo items from each of them
        <div>
            <div className="todoList">{list.map((todo, index) => (
                <Todo key={index} todo={todo} onDelete={deleteTodo} />
            ))}</div>
            {/* Form to submit a new todo */}
            <form className="form" onSubmit={handleSumbit}>
                <input className="inputForm" type="text" placeholder="Add todo..." value={todo} onChange={(e) => setTodo(e.target.value)}></input>
                <input className="btn-add" type="submit" value="Add"></input>
            </form>
        </div>
    )
}

function App() {

    return (
        <div className="App">
            <h1>To do</h1>
            <div className="todoContainer">
                <AddTodo />
            </div>
        </div>
    );
}

export default App;
